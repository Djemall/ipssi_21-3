FROM php:7.1-fpm
WORKDIR /app

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get update && apt-get install -y \
    git \
RUN curl -sL https://getcomposer.org/installer | php -- --install-dir /usr/bin --filename composer

COPY composer.json ./
RUN composer install -o

COPY package.json gulpfile.js bower.json ./
RUN npm install
RUN npm run gulp
RUN npm run rev

